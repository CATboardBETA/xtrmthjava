/*
 * Xtrmth-java, open-source math library
 * Copywrite (c) 2021 William Nelson
 * @mailto:catboardbeta AT gmail DOT com
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 *
 * Xtrmth is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

package personal.williamnelson.xtrmth.annotations;
