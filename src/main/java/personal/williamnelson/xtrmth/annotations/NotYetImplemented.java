package personal.williamnelson.xtrmth.annotations;

import personal.williamnelson.xtrmth.Utility;
import personal.williamnelson.xtrmth.exceptions.NotYetImplementedException;

import java.lang.annotation.*;

/**
 * Annotate that a function or method will simply throw a
 * {@link NotYetImplementedException},
 * Usually though {@link Utility#NYI()}
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface NotYetImplemented {}
