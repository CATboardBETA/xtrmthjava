/*
 * Xtrmth-java, open-source math library
 * Copywrite (c) 2021 William Nelson
 * @mailto:catboardbeta AT gmail DOT com
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 *
 * Xtrmth is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

package personal.williamnelson.xtrmth.exceptions;

/**
 * Xtrmth exception for when a function or
 * method is not yet implemented
 */
public class NotYetImplementedException extends RuntimeException {

    /**
     * Throw a new {@link NotYetImplementedException}.
     * @param errorMessage Message to display when {@link NotYetImplementedException} is thrown.
     */
    public NotYetImplementedException(String errorMessage) {
        super(errorMessage);
    }

    /**
     * Throw a new {@link NotYetImplementedException}.
     */
    public NotYetImplementedException() {
        super("");
    }
}
