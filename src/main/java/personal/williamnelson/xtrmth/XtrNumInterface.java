/*
 * Xtrmth-java, open-source math library
 * Copywrite (c) 2021 William Nelson
 * @mailto:catboardbeta AT gmail DOT com
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 *
 * Xtrmth is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

package personal.williamnelson.xtrmth;

/**
 * All Xtrmth numerical classes implement this interface.
 * It contains all functions in every numerical class.
 */
public interface XtrNumInterface<E> {

    boolean equals(Object other);

    // repr() and tostring() have the same result; repr simply executes toString(). 
    // It exists purely for the convenience of Python programmers,
    // where __repr__() is used to get the string representation.
    String toString();
    String repr();

    E add(Object other);
    E sub(Object other);
    E subr(Object other);
    E multiply(Object other);
    E divide(Object other);
    E divider(Object other);
    E modulo(Object other);
    E modulor(Object other);
}
