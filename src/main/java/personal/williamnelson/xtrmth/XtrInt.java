/*
 * Xtrmth-java, open-source math library
 * Copywrite (c) 2021 William Nelson
 * @mailto:catboardbeta AT gmail DOT com
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 *
 * Xtrmth is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

package personal.williamnelson.xtrmth;

import personal.williamnelson.xtrmth.annotations.NotYetImplemented;
import personal.williamnelson.xtrmth.exceptions.NoValueException;

import static personal.williamnelson.xtrmth.Utility.throwNYI;
import static personal.williamnelson.xtrmth.Utility.checkIfGenericNumberIsLegal;

import java.lang.Number;
import java.math.BigDecimal;

/**
 * XtrInt in the Xtrmth version of Integer. It contains
 * all Xtrmth functions applicable to an integer, and can easily
 * and efficiently calculate all of these.
 */
public class XtrInt extends Number implements XtrNumInterface<XtrInt>, Comparable<XtrInt>, BigDecimal {

    protected static final Class<?> JAVA_LANG_BYTE = Byte.class;
    protected static final Class<?> JAVA_LANG_BOOLEAN = Boolean.class;
    protected static final Class<?> JAVA_LANG_DOUBLE = Double.class;
    protected static final Class<?> JAVA_LANG_SHORT = Short.class;
    protected static final Class<?> JAVA_LANG_LONG = Long.class;
    protected static final Class<?> JAVA_LANG_CHARACTER = Character.class;
    protected static final Class<?> JAVA_LANG_FLOAT = Float.class;
    protected static final Class<?> JAVA_LANG_INTEGER = Integer.class;

    private final String className = this.getClass().getCanonicalName();

    private Integer val;

    /**
     * Create a new {@link XtrInt} object. They can be used to
     * calculate many math operations all within one object.
     * @param val {@link Integer} the value to set.
     */
    public XtrInt(int val) {
        this.val = val;
    }

    /**
     * Create a new XtrInt object. 
     * They can be used to calculate many math operations all within one object.
     * You must set the value manually using {@link XtrInt#setVal(Integer)}.
     */
    public XtrInt() {
        this.setVal(null);
    }

    /**
     * Get the value of the {@link XtrInt} object, as an {@link Integer}.
     * @return {@link Integer}
     */
    public final Integer getVal() throws NoValueException {
        if (val == null) {
            throw new NoValueException("XtrInt Object has not been set.");
        }
        return val;
    }

    /**
     * Set the value of your {@link XtrInt} object
     * @param val {@link Integer} the value to set.
     */
    public final void setVal(Integer val) {
        this.val = val;
    }

    /**
     * Convert the parent {@link XtrInt} to a {@link String}.
     * @return {@link String}
     */
    @Override
    public final String toString() {
        return getVal().toString();
    }

    /**
     * Convert the parameterized {@link XtrInt} to a {@link String}.
     * @param o {@link XtrInt} object to be converted.
     * @return {@link String}
     */
    public static String toString(XtrInt o) {
        return o.toString();
    }

    public final boolean equals(Object other) {

        // Check other isn't null, as a 
        // NullPointerException would be thrown in the switch block
        if (other == null) {
            throw new IllegalStateException(
                "The parameter of xtrmth.XtrMth#equals(Object) must not equal null.");
        } else
        // Explanation at bottommost paragraph above switch statement
        if (other.getClass().equals(getClass())) {
            return getVal() == (int)((XtrInt)other).getVal();
        } else {

            // The canonical name is the fully-qualified name,
            // e.g. XtrInt.getClass.getCanonicalName() would return
            // "personal.williamnelson.xtrmth.XtrInit"
            //
            // For primitives, it just returns their name
            // e.g. int.getClass().getCanonicalName() would return
            // simply "int".
            //
            // I cannot use XtrInt in the switch as it's class string
            // is defined in the final String className variable, discovered at runtime.
            // All switch case statements must be discoverable at compile time, and thus
            // it must instead be calculated in the if statement above.
            if (other.getClass() == JAVA_LANG_INTEGER) {
                return other == getVal();
            } else if (other.getClass() == JAVA_LANG_CHARACTER) {
                return Character.getNumericValue((char) other) == getVal();
            } else if (other.getClass() == JAVA_LANG_LONG) {
                return (long) other == (long) getVal();
            } else if (other.getClass() == JAVA_LANG_SHORT) {
                return (short) other == (short) (int) getVal();
            } else if (other.getClass() == JAVA_LANG_FLOAT) {
                return Utility.floatCompareEquals((float) other, (float) getVal());
            } else if (other.getClass() == JAVA_LANG_DOUBLE) {
                return Utility.doubleCompareEquals((double) other, (double) getVal());
            } else if (other.getClass() == JAVA_LANG_BOOLEAN) {
                return other.equals(getVal() > 0);
            } else if (other.getClass() == JAVA_LANG_BYTE) {
                return (byte) other == getVal().byteValue();

            } else {
                // If someone inputs an unrecognized type, 
                // it should throw an IllegalArgumentExeption.
                // Example:
                // > XtrInt foo = new XtrInt(27);
                // > SomeOtherClass bar = new SomeOtherClass();
                // > assertTrue(foo.equals(bar));
                // Would throw IllegalArgumentException at the assertion line.
                throw new IllegalArgumentException("Cannot compare " +
                        className +
                        " with " +
                        other.getClass().getCanonicalName());
            }
        }
        
    }

    /**
     * <font color="red">WARNING: hashCode cannot be used as a replacement for equals!</font>
     * {@link #equals(Object)} specially checks type, and manually checks to see with each type
     * if they are equal.
     *
     * hashCode, on the other hand, always returns the hash of the internal {@link Integer}.
     * 
     * Example: (new XtrInt(2)).equals(2) would return true, 
     * while (new XtrInt(2)).hashCode() == 2.hashCode() would return false
     * 
     * Only use with another XtrInt.
     * @deprecated
     * @return int - the hashcode
     */
    @Override
    @Deprecated(since="always")
    public int hashCode() {
        return getVal().hashCode();
    }

    /**
     * Simply calls toString.
     *
     * Not recommended for use, only exists for the convenience
     * of Python programmers, where class.__repr__() would
     * return it's representaion as an str.
     * @deprecated
     * @return {@link String}
     */
    @Override
    @Deprecated(since="always")
    public String repr() {
        return toString();
    }

    /**
     * Add the parent {@link XtrInt} to the parameterized {@link Object}
     */
    @Override
    public XtrInt add(Object other) {

        if (other.getClass().getCanonicalName().equals(className)) {
            return new XtrInt(getVal() + ((XtrInt)other).getVal());
        }

        // checkIfGenericNumberIsLegal(Object) simply checks
        // if a class is applicable to be performed with or on
        // XtrInt's 
        if (checkIfGenericNumberIsLegal(other)) {
            XtrInt ret;

            if (other.getClass() == JAVA_LANG_INTEGER) {
                ret = new XtrInt(getVal() + (Integer)other);
            } else if (other.getClass() == JAVA_LANG_FLOAT) {
                ret = new XtrInt(getVal() + ((Float)other).intValue());
            } else if (other.getClass() == JAVA_LANG_DOUBLE) {
                ret = new XtrInt(getVal() + ((Double)other).intValue());
            } else if (other.getClass() == JAVA_LANG_SHORT) {
                ret = new XtrInt(getVal() + ((Short)other).intValue());
            } else if (other.getClass() == JAVA_LANG_LONG) {
                ret = new XtrInt(getVal() + ((Long)other).intValue());
            } else {
                throw new IllegalArgumentException("Cannot add the type " + 
                    other.getClass().getCanonicalName() + 
                    " to a personal.williamnelson.xtrmth.XtrInt");
            }
            return ret;

        } else {
            // If it's not legal, than it is a type
            // not compatible with XtrInt,
            // and thus should throw an IllegalArgumentException
            throw new IllegalArgumentException("Cannot add the type" + 
                other.getClass().getCanonicalName() + 
                "to a personal.williamnelson.xtrmth.XtrInt");
        }
    }

    @Override
    public XtrInt sub(Object other) {
        
        if (other.getClass().getCanonicalName().equals(className)) {
            return new XtrInt(getVal() - ((XtrInt)other).getVal()); 
        }

        // checkIfGenericNumberIsLegal(Object) simply checks
        // if a class is applicable to be performed with or on
        // XtrInt's 
        if (checkIfGenericNumberIsLegal(other)) {
            XtrInt ret;

            if (other.getClass() == JAVA_LANG_INTEGER) {
                ret = new XtrInt(getVal() - (Integer)other);
            } else if (other.getClass() == JAVA_LANG_FLOAT) {
                ret = new XtrInt(getVal() - ((Float)other).intValue());
            } else if (other.getClass() == JAVA_LANG_DOUBLE) {
                ret = new XtrInt(getVal() - ((Double)other).intValue());
            } else if (other.getClass() == JAVA_LANG_SHORT) {
                ret = new XtrInt(getVal() - ((Short)other).intValue());
            } else if (other.getClass() == JAVA_LANG_LONG) {
                ret = new XtrInt(getVal() - ((Long)other).intValue());
            } else {
                throw new IllegalArgumentException("Cannot add the type " + 
                    other.getClass().getCanonicalName() + 
                    " to a personal.williamnelson.xtrmth.XtrInt");
            }
            return ret;

        } else {

            // If it's not legal, than it is a type
            // not compatible with XtrInt,
            // and thus should throw an IllegalArgumentException
            throw new IllegalArgumentException("Cannot subtract the type" + 
                other.getClass().getCanonicalName() + 
                "from a personal.williamnelson.xtrmth.XtrInt");
        }
    }

    @Override
    public XtrInt subr(Object other) {
                
        if (other.getClass().getCanonicalName().equals(className)) {
            return new XtrInt(getVal() - ((XtrInt)other).getVal()); 
        }

        // checkIfGenericNumberIsLegal(Object) simply checks
        // if a class is applicable to be performed with or on
        // XtrInt's 
        if (checkIfGenericNumberIsLegal(other)) {
            XtrInt ret;
        
            if (other.getClass() == JAVA_LANG_INTEGER) {
                ret = new XtrInt((Integer)other - getVal());
            } else if (other.getClass() == JAVA_LANG_FLOAT) {
                ret = new XtrInt(((Float)other).intValue() - getVal());
            } else if (other.getClass() == JAVA_LANG_DOUBLE) {
                ret = new XtrInt(((Double)other).intValue() - getVal());
            } else if (other.getClass() == JAVA_LANG_SHORT) {
                ret = new XtrInt(((Short)other).intValue() - getVal());
            } else if (other.getClass() == JAVA_LANG_LONG) {
                ret = new XtrInt(((Long)other).intValue() - getVal());
            } else {
                throw new IllegalArgumentException("Cannot add the type " + 
                    other.getClass().getCanonicalName() + 
                    " to a personal.williamnelson.xtrmth.XtrInt");
            }
            return ret;

        } else {
            // If it's not legal, than it is a type
            // not compatible with XtrInt,
            // and thus should throw an IllegalArgumentException
            throw new IllegalArgumentException("Cannot subtract the type" + 
                other.getClass().getCanonicalName() + 
                "from a personal.williamnelson.xtrmth.XtrInt");
        }
    }

    @Override
    public XtrInt multiply(Object other) {
                
        if (other.getClass().getCanonicalName().equals(className)) {
            return new XtrInt(getVal() - ((XtrInt)other).getVal()); 
        }

        // checkIfGenericNumberIsLegal(Object) simply checks
        // if a class is applicable to be performed with or on
        // XtrInt's 
        if (checkIfGenericNumberIsLegal(other)) {
            XtrInt ret;

            if (other.getClass() == JAVA_LANG_INTEGER) {
                ret = new XtrInt(getVal() * (Integer)other);
            } else if (other.getClass() == JAVA_LANG_FLOAT) {
                ret = new XtrInt(getVal() * ((Float)other).intValue());
            } else if (other.getClass() == JAVA_LANG_DOUBLE) {
                ret = new XtrInt(getVal() * ((Double)other).intValue());
            } else if (other.getClass() == JAVA_LANG_SHORT) {
                ret = new XtrInt(getVal() * ((Short)other).intValue());
            } else if (other.getClass() == JAVA_LANG_LONG) {
                ret = new XtrInt(getVal() * ((Long)other).intValue());
            } else {
                throw new IllegalArgumentException("Cannot multiply the type " + 
                    other.getClass().getCanonicalName() + 
                    " with a personal.williamnelson.xtrmth.XtrInt");
            }
            return ret;

        } else {

            // If it's not legal, than it is a type
            // not compatible with XtrInt,
            // and thus should throw an IllegalArgumentException
            throw new IllegalArgumentException("Cannot multiply the type" + 
                other.getClass().getCanonicalName() + 
                " with a personal.williamnelson.xtrmth.XtrInt");
        }
    }

    @NotYetImplemented
    @Override
    public XtrInt divide(Object other) {
        throwNYI();
        return null;
    }

    @NotYetImplemented
    @Override
    public XtrInt divider(Object other) {
        throwNYI();
        return null;
    }

    @NotYetImplemented
    @Override
    public XtrInt modulo(Object other) {
        throwNYI();
        return null;
    }

    @NotYetImplemented
    @Override
    public XtrInt modulor(Object other) {
        throwNYI();
        return null;
    }

    @Override
    public int compareTo(XtrInt other) {
        return compare(getVal(), other.getVal());
    }

    public static int compare(int v1, int v2) {
        return (v1 < v2) ? -1 : ((v1 == 2) ? 0 : 1);
    }

    @Override
    public int intValue() {
        return getVal();
    }

    @Override
    public long longValue() {
        return (long)getVal();
    }

    @Override
    public float floatValue() {
        return (float)getVal();
    }

    @Override
    public double doubleValue() {
        return (double)getVal();
    }
}
