/*
 * Xtrmth-java, open-source math library
 * Copywrite (c) 2021 William Nelson
 * @mailto:catboardbeta AT gmail DOT com
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 *
 * Xtrmth is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */
 
package personal.williamnelson.xtrmth;

import personal.williamnelson.xtrmth.exceptions.NoValueException;
import personal.williamnelson.xtrmth.exceptions.NotYetImplementedException;

public final class Utility {

    private Utility() {
        // No need to do anything. This constructor only 
        // exists to remove the default one.
        // 
        // A utility class should not be instantiatable.
    }

    /**
     * Helper method to make throwing
     * {@link NotYetImplementedException} easier.
     */
    static void throwNYI() {
        StackTraceElement callerOfCaller = getCallerOfCaller();
        throw new NotYetImplementedException("The function '" +
                callerOfCaller.getClassName() +
                "#" +
                callerOfCaller.getMethodName() +
                "' is not yet implemented");
    }

    /**
     * Check if an {@link Object}
     * is valid for a number.
     * @param x The Object to check if valid
     * @return Whether or not the parameter is valid
     */
    static boolean checkIfGenericNumberIsLegal(Object x) {
        if (x == null) {
            // If the Object is null, than it doesn't yet have a value, 
            // and isn't legal.
            throw new NoValueException("The value passed into " + getCallerOfCaller() + " equals null.");
        }

        switch (x.getClass().getCanonicalName()) {
            // It should be noted that none of the
            // primitives should ever be present
            // in this function, as it inputs
            // an Object. If a primitive is inputted,
            // it will be autoboxxed to it's class equivalent.
            case "int":
            case "float":
            case "double":
            case "short":
            case "long":
            case "java.lang.Integer":
            case "java.lang.Double":
            case "java.lang.Float":
            case "java.lang.Short":
            case "java.lang.Long":
            case "personal.williamnelson.xtrmth.XtrInt":
                // Valid type
                return true;
            default:
                // Invalid type
                return false;
        }
    }

    static StackTraceElement getCallerOfCaller() {
        // current function = 1, caller = 2, caller of caller = 3
        final int ELEMENT = 3;

        // Grab the full Stack Trace
        StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();
        return stacktrace[ELEMENT];
    }


    static boolean floatCompareEquals(Float a, Float b) {
        final var THRESHOLD = 0.0001F;

        return (a - b < THRESHOLD) && (a - b) > 0 - THRESHOLD;
    }

    static boolean doubleCompareEquals(Double a, Double b) {
        final var THRESHOLD = 0.0001D;

        return (a - b < THRESHOLD) && (a - b) > 0 - THRESHOLD;
    }
}
