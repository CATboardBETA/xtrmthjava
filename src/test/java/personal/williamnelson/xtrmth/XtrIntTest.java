/*
 * Xtrmth-java, open-source math library
 * Copywrite (c) 2021 William Nelson
 * @mailto:catboardbeta AT gmail DOT com
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 *
 * Xtrmth is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

package personal.williamnelson.xtrmth;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import personal.williamnelson.xtrmth.exceptions.NoValueException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit tests for XtrInt
 */
class XtrIntTest {

    private final XtrInt always3 = new XtrInt(3);

    // Test with 0
    @Test
    void testSetXtrInt1() throws NoValueException {
        XtrInt x = new XtrInt(0);
        assertEquals(0, (int)x.getVal(), "x's value should equal 0.");
    }

    // Test with negative
    @Test
    void testSetXtrInt2() throws NoValueException {
        XtrInt x = new XtrInt(-29);
        assertEquals(-29, (int)x.getVal(), "x's value should equal -29.");
    }

    // Test with positive
    @Test
    void testSetXtrInt3() throws NoValueException {
        XtrInt x = new XtrInt(43);
        assertEquals(43, (int)x.getVal(), "x's value should equal 43.");
    }

    @Test
    void testSetNullXtrInt() {
        XtrInt x = new XtrInt();
        try {
            x.getVal();

            Assertions.fail("Did not throw NoValueException");
        } catch (NoValueException e) {
            // It passed
        }
    }


    @Test
    void testToString1() {
        XtrInt x = new XtrInt(25);
        assertEquals("25", x.toString(),
                "x.toString should return the value of X as a String");
    }

    @Test
    void testToString2() {
        XtrInt x = new XtrInt(256);
        assertEquals("256", XtrInt.toString(x),
                "Calling the static version of XtrInt#toString should also return x as a String");
    }

    @Test
    void testEquals1() {
        XtrInt x = new XtrInt(252);
        try {
            x.equals(null);
            // If it doesn't throw an exception, it failed
            fail("XtrInt.equals(null) should throw a java.lang.IllegalStateException");
        } catch (IllegalStateException e) {
            // It passed
        }
    }

    @Test
    void testEquals2() {
        XtrInt x = new XtrInt(252);
        XtrInt y = new XtrInt(252);
        assertEquals(x, y, "Two XtrInt's assigned the same should be equal.");
    }

    @Test
    void testEquals3() {
        XtrInt x = new XtrInt(2);
        XtrInt y = new XtrInt(3);
        assertNotEquals(x, y, "Two XtrInt's assigned to different values should be not equal.");
    }

    @Test
    void testEquals4() {
        boolean b = always3.equals(3F);
        assertTrue(b);
    }
    
    @Test
    void testEquals5() {
        boolean b = always3.equals(3D);
        assertTrue(b);
    }

    @Test
    void testEquals6() {
        boolean b = always3.equals(3);
        assertTrue(b);
    }

    @Test
    void testEquals7() {
        boolean b = always3.equals('3');
        assertTrue(b);
    }

    @Test
    void testEquals8() {
        boolean b = always3.equals(3L);
        assertTrue(b);
    }

    @Test
    void testEquals9() {
        boolean b = always3.equals((short)3);
        assertTrue(b);
    }
    
    @Test
    void testEquals10() {
        boolean b = always3.equals(true);
        assertTrue(b);
    }

    @Test
    void testEquals11() {
        boolean b = always3.equals(((Integer)3).byteValue());
        assertTrue(b);
    }

    @Test
    void testEquals12() {
        try {
            boolean b = always3.equals(new XtrIntTest());
            fail(b + " should've been false.");
        } catch (IllegalArgumentException e) {
            // Passed
        }
    }

    @Test
    @SuppressWarnings("deprecation")
    void testHashCode() {
        assertEquals(always3.hashCode(), ((Integer)3).hashCode());
    }

    @Test
    @SuppressWarnings("deprecation")
    void testRepr() {
        assertEquals(always3.repr(), always3.toString());
    }

    @Test
    void testAdd() {
        XtrInt x1 = new XtrInt(23);
        XtrInt x2 = new XtrInt(3);
        assertEquals(26, (x1.add(x2)).getVal());
    }
}
